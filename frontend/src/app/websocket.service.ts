  import { Injectable, EventEmitter } from '@angular/core';
import ReconnectingWebSocket from 'reconnecting-websocket';

@Injectable()
export class SocketService {

    private socket: ReconnectingWebSocket;
    private listener: EventEmitter<any>
    // LOCAL
    private url =  'ws://localhost:80/v1/temperature-live'
    // PROD
    // private url =  'wss://iot-usp-query.azurewebsites.net/v1/temperature-live'

    public constructor() {}

    public connect(){
      this.socket = new ReconnectingWebSocket(this.url);
      this.socket.onopen = event => {
        this.listener.emit({"type": "open", "data": event});
        // this.waitForSocketConnection(this.socket, 5, () => {
        //     this.listener.emit({"type": "open", "data": event});
        //   });
      }
      this.socket.onclose = event => {
          this.listener.emit({"type": "close", "data": event});
      }
      this.socket.onmessage = event => {
          this.listener.emit({"type": "message", "data": JSON.parse(event.data)});
      }
      this.socket.onerror = event => {
        // const errorMsg =  `Ops, some connection error @: ${this.url} \n Check the backend  \n ERROR:  ${event} `
        const errorMsg =  `Ops, some connection error @: ${this.url} \nCheck the backend  \nERROR:  SOCKET ERROR EVENT `
        console.log(errorMsg)
        this.listener.emit({"type": "error", "data": errorMsg});
      };
    }

    private waitForSocketConnection(socket, interval, callback) {
      setTimeout(() => {
        if (socket.readyState === 1) {
          if (callback !== undefined) {
            callback();
          }
          return;
        } else {
          this.waitForSocketConnection(socket, interval, callback);
        }
      }, interval);
    }

    public send(data: string) {
        this.socket.send(data);
    }

    public close() {
        this.socket.close();
    }

    public getEventListener() {
      this.listener = new EventEmitter();
      return this.listener;
    }
    public destroyEventListener(){
      this.listener.unsubscribe()
    }

}
