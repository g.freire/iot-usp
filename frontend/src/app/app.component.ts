import { Component } from '@angular/core';
import { SocketService } from './websocket.service';



@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
 public sensorResult;
 public timestampResult;
 public temperatureResult;
 public chartDataSchemaRPMGauge;

  // gauge options -----------------------
  gaugeView: any[] = [750, 300]
  gaugeColorSchemePie = 'forest'
  gaugeMin = 0
  gaugeMax = 45
  // gaugeUnits = "Temp. °C"
  gaugeStartAngle = -90
  gaugeEndAngle = 180
  // gaugeLegend = true
 gaugeLegendTitle = "Generated Power"

  gaugeColorSchemePie2 = 'ocean'

 constructor(
    private socketService: SocketService,
  ) {}

  async ngOnInit() {
    this.startStreaming();
  }

  startStreaming() {
    this.socketService.connect();
    this.socketService.getEventListener().subscribe(event => {
      if (event.type == 'message') {
        this.sensorResult = event.data[0]['Sensor'];
        this.timestampResult = event.data[0]['Key'].slice(0,19);
        this.temperatureResult = event.data[0]['Value'];
        // const filteredResult = ChartUtils.filterDataByMotorType(this.motorStreamResult, 'ENCODER');
        // this.processEncoderRealTimeData(filteredResult)

        // Create gauge 3 indicators
        this.chartDataSchemaRPMGauge = [
          {
            "name": "temperatureResult",
            "value": +this.temperatureResult
          },
          // {
          //   "name": "temperatureResult",
          //   "value": this.temperatureResult
          // },
          // {
          //   "name": "temperatureResult",
          //   "value": this.temperatureResult
          // },
        ];


        ;
      }
      if (event.type == 'close') {
        console.log('/The socket connection has been closed');
      }
      if (event.type == 'open') {
        console.log('/The socket connection has been established');
      }
    });
    //
  }

  stopStreaming() {
    console.log('stop temperature feed');
    this.temperatureResult = [];
    this.socketService.close();
    this.socketService.getEventListener().next();
    this.socketService.getEventListener().complete();
  }

  ngOnDestroy(): void {
    console.log('DESTROYING ENCODER COMPONENT');
    this.stopStreaming();
  }

}
