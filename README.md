# IoT-USP

Steps to Run
```diff

1) Device + Sensor (ESP32 + DHT22)
Use Arduino IDE Tools > Serial Monitor to debug
Run esp32/dht22+mqtt.ino file

2) Broker
Build the  mosquitto or emqtt instances using Docker local or cloudMQTT
$ docker run -d -p 1883:1883 --name myEmqtt raymondmm/emqtt
$ docker run -it -p 1883:1883 eclipse-mosquitto mosquitto -c /mosquitto-no-auth.conf

3) Write Sensor Microservice (Golang)
Run the golang code at backend_write_sensor_folder
$ go run cmd/write_sensor/main.go

4) Database (Mongo)
Run the mongo instance local 
$ docker run -d -p 27017:27017 --name db mongo
Cloud: point to the AWS implementation (default)

5) Query Sensor Microservice (Golang)
Run the golang code at backend_query
$ go run cmd/query/main.go


6) Frontend
Run the angular local
$ npm start
The production link can be found at https://iot-usp.herokuapp.com/
```
![alt text](./Architecture.png)