module iot

go 1.16

require (
	github.com/eclipse/paho.mqtt.golang v1.3.5
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-contrib/gzip v0.0.3
	github.com/gin-gonic/gin v1.7.2
	github.com/influxdata/influxdb-client-go/v2 v2.4.0
	github.com/leodido/go-urn v1.2.1 // indirect
	go.mongodb.org/mongo-driver v1.7.0
	gopkg.in/go-playground/validator.v9 v9.31.0
)
