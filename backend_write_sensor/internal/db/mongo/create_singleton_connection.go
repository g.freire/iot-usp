package mongo

import (
	"context"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"sync"
	"time"
)

//  CONCURRENT SINGLETON DB CONNECTION
type MongoConnection struct {
	Conn *mongo.Client
}

func ConnectMongo(host string) *mongo.Client {
	var once sync.Once
	var instance *MongoConnection

	once.Do(func() {
		log.Printf("SINGLETON CONCURRENT MONGODB CONNECTION CREATED")
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		clientConnection, err := mongo.Connect(ctx, options.Client().ApplyURI(host).
			SetMaxPoolSize(300).
			SetConnectTimeout(120*time.Second).
			SetServerSelectionTimeout(120*time.Second).
			SetSocketTimeout(120*time.Second))
		if err != nil {
			log.Printf("SINGLETON CONCURRENT DB CONNECTION ERROR !!")
			log.Fatal(err)
		}
		instance = &MongoConnection{Conn: clientConnection}
	})
	return instance.Conn
}
