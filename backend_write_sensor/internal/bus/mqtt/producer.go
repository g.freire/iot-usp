package mqtt

import (
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"strconv"
	"time"
)


func Produce(client mqtt.Client, topic string) {
	for i := 0; ; i++ {
		token := client.Publish(topic, 0, false, "MSG: "+strconv.Itoa(i))
		token.Wait()
		fmt.Printf("PUBLISHED * [%s] %s\n", topic, "MSG: "+strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}

