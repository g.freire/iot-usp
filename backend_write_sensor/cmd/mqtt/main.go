package main

import (
	"flag"
	"fmt"
	"log"
	"net/url"
	"strconv"
	"time"

	mqtt "github.com/eclipse/paho.mqtt.golang"
)

var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())
}



func connect(clientId string, uri *url.URL) mqtt.Client {
	opts := createClientOptions(clientId, uri)
	client := mqtt.NewClient(opts)
	token := client.Connect()
	for !token.WaitTimeout(3 * time.Second) {
	}
	if err := token.Error(); err != nil {
		log.Fatal(err)
	}
	return client
}

func createClientOptions(clientId string, uri *url.URL) *mqtt.ClientOptions {
	opts := mqtt.NewClientOptions()
	opts.AddBroker(fmt.Sprintf("tcp://%s", uri.Host))
	opts.SetUsername(uri.User.Username())
	password, _ := uri.User.Password()
	opts.SetPassword(password)
	opts.SetClientID(clientId)
	return opts
}


func publish(client mqtt.Client, topic string) {
	for i := 0; ; i++ {
		client.Publish(topic, 0, false, "MSG: "+strconv.Itoa(i))
		fmt.Printf("PUBLISHED * [%s] %s\n", topic, "MSG: "+strconv.Itoa(i))
		time.Sleep(time.Second)
	}
}

func subscribe(client mqtt.Client, topic string) {
	token := client.Subscribe(topic, 1, messagePubHandler)
	token.Wait()
	fmt.Printf("Subscribed to topic %s", topic)
	for{}
}

func listen(uri *url.URL, topic string) {
	client := connect("sub", uri)
	client.Subscribe(topic, 0, func(client mqtt.Client, msg mqtt.Message) {
		fmt.Printf("* [%s] %s\n", msg.Topic(), string(msg.Payload()))
	})
}

func main() {
	//RUN A CONSUMER go run main.go -t=c -id=1
	//RUN A PRODUCER go run main.go -t=p -id=1
	//log
	//mqtt.DEBUG = log.New(os.Stdout, "[DEBUG] ", 0)

	// CLI
	client_type := flag.String("t", "c", "Client type eg. -t=c for consumer or -t=p for producer")
	id := flag.Int("id", 1, "ID of the consumer or producer ")
	flag.Parse()
	log.Println("  CLIENT TYPE:", *client_type, "ID:", *id)


	//uri, err := url.Parse(os.Getenv("CLOUDMQTT_URL"))
	//export CLOUDMQTT_URL=mqtt://<user>:<pass>@<write_sensor>.cloudmqtt.com:<port>/<topic>
	//url := "mqtt://nhgmjuev:52Kys-PYJnUQ@driver.cloudmqtt.com:18763/test"
	uri, err := url.Parse("mqtt://nhgmjuev:52Kys-PYJnUQ@driver.cloudmqtt.com:18763")
	// uri, err := url.Parse("mqtt://localhost:1883")
	//uri, err := url.Parse("mqtt://192.168.0.103:1883")

	if err != nil {
		log.Fatal(err)
	}
	topic := "temperature"
	//go listen(uri, topic)


	if *client_type == "p" {
		client := connect("pub", uri)
		publish(client, topic)
	}
	if *client_type == "c" {
		client := connect("sub", uri)
		subscribe(client, topic)
	}

}
