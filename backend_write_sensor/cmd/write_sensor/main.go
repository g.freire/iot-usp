package main

import (
	"iot/internal/bus/mqtt"
	"net/url"

	"log"
)

const(
	HOST_MQTT = "mqtt://nhgmjuev:52Kys-PYJnUQ@driver.cloudmqtt.com:18763"
	TOPIC = "temperature"
)


func main() {

	// MONGO DB SETUP
	//hostMONGO := "mongodb://fair-admin:fair!2021@184.72.196.91:27017/?authSource=fair"
	//dbConnMongo := mongo.ConnectMongo(hostMONGO)
	//if err := dbConnMongo.Ping(context.TODO(), readpref.Primary()); err != nil {
	//	log.Fatal(err)
	//}
	//repository := NewTempMongoRepository(dbConnMongo, "IOT_QA", "TEMP")

	// MQTT SETUP
	uri, err := url.Parse(HOST_MQTT)
	if err != nil {
		log.Fatal(err)
	}
	client := mqtt.Connect("sub", uri)
	mqtt.Consume(client, TOPIC)
}
