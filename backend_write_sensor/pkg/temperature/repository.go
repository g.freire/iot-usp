package temperature

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/mongo"
	"time"
)

type TempMongoRepository struct {
	Conn *mongo.Client
	Db   string
	Coll string
}

var genericCtxTimeoutDuration = 40 * time.Second

func NewTempMongoRepository(Conn *mongo.Client, Db, Coll string) *TempMongoRepository {
	return &TempMongoRepository{Conn, Db, Coll}
}

func (m *TempMongoRepository) Save(document Temperature) (interface{}, error) {
	var Ctx, _ = context.WithTimeout(context.Background(), genericCtxTimeoutDuration)
	collection := m.Conn.Database(m.Db).Collection(m.Coll)
	result, err := collection.InsertOne(Ctx, document)
	if err != nil {
		return nil, err
	}
	newID := result.InsertedID
	fmt.Println("InsertOne() newID:", newID)
	return newID, nil

}