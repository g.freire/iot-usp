package main

import (
	"context"
	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"gopkg.in/go-playground/validator.v9"
	mongo "iot/internal/db/mongo"
	. "iot/pkg/temperature"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func handleVersion(c *gin.Context) {
	c.JSON(http.StatusOK, "IOT USP v1.0 - 2021-07-23")
}

func main() {
	// MONGO DB SETUP
	hostMONGO := "mongodb://fair-admin:fair!2021@184.72.196.91:27017/?authSource=fair"
	dbConnMongo := mongo.ConnectMongo(hostMONGO)
	if err := dbConnMongo.Ping(context.TODO(), readpref.Primary()); err != nil {
		log.Fatal(err)
	}
	repository := NewTempMongoRepository(dbConnMongo, "IOT_QA", "TEMP")

	// WEB SERVER SETUP
	PORT := "80"
	r := gin.Default()
	r.Use(cors.Default())
	r.Use(gzip.Gzip(gzip.DefaultCompression))
	r.GET("/", handleVersion)

	// WEBSOCKET
	go Pool.Start(*repository)
	ApplyRoutes(r)

	srv := &http.Server{
		Addr:    ":" + PORT,
		Handler: r,
	}

	validator := validator.New()
	NewHandler(
		r, "temperature",
		validator,
		repository,
	)

	// GRACEFULL SHUTDOWNS

	//DBs
	defer func() {
		ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
		defer cancel()
		err := dbConnMongo.Disconnect(ctx)
		if err != nil {
			log.Fatal(err)
		}
	}()

	//SERVER
	go func() {
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
	quit := make(chan os.Signal)
	signal.Notify(quit, syscall.SIGINT, syscall.SIGTERM)
	<-quit
	log.Println("Shutting down server...")
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server forced to shutdown:", err)
	}
	log.Println("Server exiting")
}
