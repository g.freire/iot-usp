package mqtt

import (
	"context"
	"fmt"
	mqtt "github.com/eclipse/paho.mqtt.golang"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"iot/internal/db/mongo"
	temperature "iot/pkg/temperature"
	"log"
)


var messagePubHandler mqtt.MessageHandler = func(client mqtt.Client, msg mqtt.Message) {
	fmt.Printf("Received message: %s from topic: %s\n", msg.Payload(), msg.Topic())

	// MONGO DB SETUP
	hostMONGO := "mongodb://fair-admin:fair!2021@184.72.196.91:27017/?authSource=fair"
	dbConnMongo := mongo.ConnectMongo(hostMONGO)
	if err := dbConnMongo.Ping(context.TODO(), readpref.Primary()); err != nil {
		log.Fatal(err)
	}
	repository := temperature.NewTempMongoRepository(dbConnMongo, "IOT_QA", "TEMP")
	tempEntity := temperature.Temperature{"DHT22", string(msg.Payload()), "a"}
	repository.Save(tempEntity)
}

func Consume(client mqtt.Client, topic string, repository *temperature.TempMongoRepository) {
	token := client.Subscribe(topic, 1, messagePubHandler)

	token.Wait()
	fmt.Printf("Subscribed to topic %s", topic)

}
