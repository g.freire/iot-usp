#### PROD

#### AZURE   
# https://iot-usp-query.azurewebsites.net/            
# az login --username gustavomfreire@outlook.com --password <senha>
# az acr login --name iotUspQuery
# az acr update -n iotUspQuery --admin-enabled true

#### DOCKER
docker build --rm -f "Dockerfile" -t iotusp:v1 "."
docker tag iotusp:v1 iotuspquery.azurecr.io/iotusp:v1
docker push iotuspquery.azurecr.io/iotusp:v1
az webapp log tail --name iot-usp-query --resource-group iot-usp

# IPs
# 13.86.3.178/32
# 13.86.56.19/32
# 13.86.57.184/32
# 13.86.59.58/32
# 13.86.60.95/32
# 52.154.157.16/32
# 20.40.202.19/32