package temperature

import (
	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
)

type Handler struct {
	Repository *TempMongoRepository
	Validate   *validator.Validate
}

func NewHandler(r *gin.Engine, route string, val *validator.Validate, tr *TempMongoRepository) {
	handler := &Handler{
		Repository: tr,
		Validate:   val,
	}
	v1 := r.Group("v1/" + route)
	{
		//v1.GET("", handler.GetAll)
		//v1.GET("/:id", handler.GetByID)
		v1.POST("", handler.Save)
	}
}

func (t *Handler) Save(c *gin.Context) {
	var requestBody Temperature
	c.BindJSON(&requestBody)
	if err := t.Validate.Struct(requestBody); err != nil {
		c.JSON(http.StatusNotFound, c.Error(err))
		return
	}
	_, err := t.Repository.Save(requestBody)
	if err != nil {
		c.JSON(http.StatusNotFound, c.Error(err))
	} else {
		c.JSON(http.StatusOK, requestBody)
	}
}