package temperature

type Temperature struct {
	Sensor string
	Key    string
	Value  string
}
