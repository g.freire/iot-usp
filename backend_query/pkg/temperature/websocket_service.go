package temperature

import (
	"fmt"
	"time"
)

func GetLastTemperatureSamples(repository TempMongoRepository, stopchan <-chan bool){
	for {
		select {
		default:
			var size int64 = 1
			result, err := repository.GetLastSamples(size, true)
			if err == nil && len(result) > 0{
				fmt.Print("\n BROADCASTING TOTAL OBJS: ", size)
				fmt.Print("\n RESULT[0]: ", result[0], "\n")
				Pool.Broadcast <- result
				time.Sleep(1000 * time.Millisecond)
			}else{
				fmt.Print(result, err)
			}
		case <-stopchan:
			fmt.Print("\n CLOSING INFINITE QUERY LOOP \n")
			return
		}
	}
}