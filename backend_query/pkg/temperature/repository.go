package temperature

import (
	"context"
	"fmt"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"log"
	"time"
)

type TempMongoRepository struct {
	Conn *mongo.Client
	Db   string
	Coll string
}

var genericCtxTimeoutDuration = 40 * time.Second

func NewTempMongoRepository(Conn *mongo.Client, Db, Coll string) *TempMongoRepository {
	return &TempMongoRepository{Conn, Db, Coll}
}

func (m *TempMongoRepository) Save(document Temperature) (interface{}, error) {
	var Ctx, _ = context.WithTimeout(context.Background(), genericCtxTimeoutDuration)
	collection := m.Conn.Database(m.Db).Collection(m.Coll)
	result, err := collection.InsertOne(Ctx, document)
	if err != nil {
		return nil, err
	}
	newID := result.InsertedID
	fmt.Println("InsertOne() newID:", newID)
	return newID, nil
}

func (m *TempMongoRepository) GetLastSamples(size int64, sort_last bool) ([]Temperature, error) {
	var Temp Temperature
	var Temps []Temperature
	var Ctx, _ = context.WithTimeout(context.Background(), genericCtxTimeoutDuration)
	collection := m.Conn.Database(m.Db).Collection(m.Coll)
	count, err := collection.CountDocuments(Ctx, bson.D{{}})
	fmt.Print("\n ", collection, " SIZE: ", count)
	options := options.Find()
	if sort_last {
		options.SetSort(bson.D{{"_id", -1}})
	}
	options.SetLimit(size)
	cur, err := collection.Find(Ctx, bson.M{}, options)
	if err != nil {
		fmt.Print(err)
		p := new ([]Temperature)
		return *p, err
	}else{
		// create array of temperatures
		for cur.Next(Ctx) {
			err := cur.Decode(&Temp)
			if err != nil {
				log.Println(err)
			}
			Temps = append(Temps, Temp)
		}
	}
	return Temps, nil
}