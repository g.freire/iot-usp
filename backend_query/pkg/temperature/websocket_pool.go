package temperature

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

type Client struct {
	Id         string
	Ip         string
	Socket     *websocket.Conn
	ClientChan chan interface{}
}

type PoolStructure struct {
	Register   chan *Client
	Unregister chan *Client
	Broadcast  chan interface{}
	Clients    map[*Client]bool
}

var Pool = PoolStructure{
	Register:   make(chan *Client),
	Unregister: make(chan *Client),
	Broadcast:  make(chan interface{}),
	Clients:    make(map[*Client]bool),
}

func (pool *PoolStructure) Start(repository TempMongoRepository) {
	var jobChan = make(chan bool)
	var pollSize int
	for {
		select {
		case client := <-pool.Register:
			pool.Clients[client] = true
			pollSize = len(pool.Clients)
			fmt.Print("## POOL SIZE NOW: ", pollSize, "\n")
			if pollSize == 1 {
				fmt.Print("** CREATING THE SINGLETON JOB \n")
				jobChan = make(chan bool)
				go GetLastTemperatureSamples(repository, jobChan)
			}
		case client := <-pool.Unregister:
			if _, ok := pool.Clients[client]; ok {
				close(client.ClientChan)
				delete(pool.Clients, client)
				fmt.Println("A socket has disconnected.")
				pollSize = len(pool.Clients)
				fmt.Print("## POOL SIZE NOW: ", pollSize, "\n")
				if pollSize == 0 {
					fmt.Print("** CLOSING THE SINGLETON JOB \n")
					close(jobChan)
					//jobChan <- true
				}
			}
		case message := <-pool.Broadcast:
			for client := range pool.Clients {
				select {
				case client.ClientChan <- message:
				default:
					close(client.ClientChan)
					delete(pool.Clients, client)
				}
			}
		}
	}
}


func (c *Client) Read(ctx *gin.Context) {
	defer func() {
		Pool.Unregister <- c
		c.Socket.Close()
	}()
	for {
		_, _, err := c.Socket.ReadMessage()
		if err != nil {
			Pool.Unregister <- c
			c.Socket.Close()
			break
		}
	}
}

func (c *Client) Write(client *Client) {
	defer func() {
		c.Socket.Close()
	}()

	for {
		select {
		case message, ok := <-c.ClientChan:
			if !ok {
				c.Socket.WriteMessage(websocket.CloseMessage, []byte{})
				return
			}
			//fmt.Println(message)
			//msg := message.(domain.Order)
			c.Socket.WriteJSON(message)
			//fmt.Println("SENDING ", msg.Type," IP: ", client.Ip," ID ", client.Id)
		}
	}
}