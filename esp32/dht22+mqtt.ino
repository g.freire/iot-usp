#include <WiFi.h>
#include <PubSubClient.h>
#include "DHT.h"



// WIFI
const char* ssid = "Proxy"; // Enter your WiFi name
const char* password =  "gfgfgfgf"; // Enter WiFi password

// MQTT
const char* mqtt_server = "driver.cloudmqtt.com"; // enter mqtt server 
const int mqtt_port = 18763; //enter mqtt port
#define MQTT_USER "nhgmjuev"
#define MQTT_PASSWORD "52Kys-PYJnUQ"
#define MQTT_SERIAL_PUBLISH_CH "pub"
#define MQTT_SERIAL_RECEIVER_CH "sub"
#define TOPIC_NAME_A "temperature"
#define TOPIC_NAME_B "humidity"

WiFiClient wifiClient;
PubSubClient client(wifiClient);

// DHT22
#define DHTPIN 5     // Digital pin connected to the DHT sensor
#define DHTTYPE DHT22   // DHT type
DHT dht(DHTPIN, DHTTYPE);
// Variables for printing temp and humidity
String temp_str; //see last code block below use these to convert the float that you get back from DHT to a string =str
String hum_str;
char temp[50];
char hum[50];


void setup() {
  Serial.begin(9600);
  Serial.setTimeout(500);// Set time out for 
  setup_wifi();
  Serial.println(F("DHT22 start!"));
  dht.begin();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();
}

void setup_wifi() {
    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    randomSeed(micros());
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}


void callback(char* topic, byte *payload, unsigned int length) {
    Serial.println("-------new message from broker-----");
    Serial.print("channel:");
    Serial.println(topic);
    Serial.print("data:");  
    Serial.write(payload, length);
    Serial.println();
}


void publishSerialData(char *serialData){
  if (!client.connected()) {
    reconnect();
  }
  client.publish(MQTT_SERIAL_PUBLISH_CH, serialData);
}
void loop() {
   client.loop();
   if (Serial.available() > 0) {
     char mun[501];
     memset(mun,0, 501);
     Serial.readBytesUntil( '\n',mun,500);
     publishSerialData(mun);
   }
 }

 
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(),MQTT_USER,MQTT_PASSWORD)) {
      Serial.println("connected");
      //Once connected, publish an announcement...
      // String msg =  "hello world";

      ///////////////////////////////////////////////////////////////////
     // GET DHT 22 TEMP AND PUBLISH TO BROKER
     ///////////////////////////////////////////////////////////////////
      while (true) {
          // dht22
          float t_Celsius = dht.readTemperature();
          float h = dht.readHumidity();
          if (isnan(t_Celsius) || isnan(h)) {
              Serial.println(("Failed to read from DHT sensor!"));
              return;
            }
          Serial.println(("Temperature: "));
          Serial.println(t_Celsius);
          Serial.print(F("Humidity: "));
          Serial.println(h);
          // type conversions
          temp_str = String(t_Celsius); //converting ftemp (the float variable above) to a string 
          temp_str.toCharArray(temp, temp_str.length() + 1); //packaging up the data to publish to mqtt 
          hum_str = String(h); //converting Humidity (the float variable above) to a string
          hum_str.toCharArray(hum, hum_str.length() + 1); //packaging up the data to publish to mqtt 

          // broker
          client.publish(TOPIC_NAME, temp, 0);
          Serial.println("published");
          delay(3000);
      }
      // ... and resubscribe
      client.subscribe(MQTT_SERIAL_RECEIVER_CH);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}
