#include <WiFi.h>
#include <PubSubClient.h>


// Update these with values suitable for your network.
const char* ssid = "Proxy";
const char* password = "gfgfgfgf";
// const char* mqtt_server = "driver.cloudmqtt.com";
// #define mqtt_port 18763
// #define MQTT_USER "nhgmjuev"
// #define MQTT_PASSWORD "52Kys-PYJnUQ"
// #define MQTT_SERIAL_PUBLISH_CH "a"
// #define MQTT_SERIAL_RECEIVER_CH "b"


// Wireless LAN adapter Wi-Fi:
//    Connection-specific DNS Suffix  . : home
//    Link-local IPv6 Address . . . . . : fe80::2008:750b:aea:ee1e%29
//    IPv4 Address. . . . . . . . . . . : 192.168.0.103

const char* mqtt_server = "192.168.0.103";
#define mqtt_port 1883
// #define MQTT_USER "admin"
// #define MQTT_PASSWORD "admin"
#define MQTT_USER ""
#define MQTT_PASSWORD ""
#define MQTT_SERIAL_PUBLISH_CH "a"
#define MQTT_SERIAL_RECEIVER_CH "b"



WiFiClient wifiClient;

PubSubClient client(wifiClient);

void setup_wifi() {
    delay(10);
    // We start by connecting to a WiFi network
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(ssid);
    WiFi.begin(ssid, password);
    while (WiFi.status() != WL_CONNECTED) {
      delay(500);
      Serial.print(".");
    }
    randomSeed(micros());
    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      //Once connected, publish an announcement...
     
      while(true){
        client.publish("test", "hello world");
        Serial.println("Published");
        delay(1000);
      }

      client.publish("test", "hello world");
      Serial.println("Published");
            
      
      // ... and resubscribe
      client.subscribe(MQTT_SERIAL_RECEIVER_CH);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte *payload, unsigned int length) {
    Serial.println("-------new message from broker-----");
    Serial.print("channel:");
    Serial.println(topic);
    Serial.print("data:");  
    Serial.write(payload, length);
    Serial.println();
}

void setup() {
  Serial.begin(9600);
  Serial.setTimeout(500);// Set time out for 
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();
}

void publishSerialData(char *serialData){
  if (!client.connected()) {
    reconnect();
  }
  client.publish(MQTT_SERIAL_PUBLISH_CH, serialData);
}
void loop() {
   client.loop();
   if (Serial.available() > 0) {
     char mun[501];
     memset(mun,0, 501);
     Serial.readBytesUntil( '\n',mun,500);
     publishSerialData(mun);
   }
 }